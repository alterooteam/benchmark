# -*- coding: iso-8859-15 -*-
"""ep FunkLoad test

$Id: $
"""
import unittest
from funkload.FunkLoadTestCase import FunkLoadTestCase
from webunit.utility import Upload
from funkload.utils import Data
from funkload.utils import xmlrpc_get_credential

class Ep(FunkLoadTestCase):
    """XXX

    This test use a configuration file Ep.conf.
    """

    def setUp(self):
        """Setting up test."""
        self.logd("setUp")
        self.server_url = self.conf_get('main', 'url')
        # XXX here you can setup the credential access like this
        credential_host = self.conf_get('credential', 'host')
        credential_port = self.conf_getInt('credential', 'port')
        self.login, self.password = xmlrpc_get_credential(credential_host,
                                          credential_port)
                                          

    def test_ep(self):
        # The description should be set in the configuration file
        #server_url = self.server_url
        # begin of test ---------------------------------------------

        # /tmp/tmp3E28ET_funkload/watch0002.request
        #self.get("http://alteroo.com/","")
        self.get("http://104.237.131.208/ep",
            description="Get /ep")
        #self.get("http://alteroo.com",description="alteroo")
        # /tmp/tmp3E28ET_funkload/watch0009.request
        #self.get("http://23.239.19.202/ep/@@usergroup-userprefs",
        #    description="Get /ep/@@usergroup-userprefs")
        # /tmp/tmp3E28ET_funkload/watch0016.request
        # end of test -----------------------------------------------

    def tearDown(self):
        """Setting up test."""
        self.logd("tearDown.\n")



if __name__ in ('main', '__main__'):
    unittest.main()
